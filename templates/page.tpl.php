<div id="page" class="off-canvas-wrap <?php print $classes; ?>"<?php print $attributes; ?> data-offcanvas>
<div class="inner-wrap">

	<nav class="tab-bar hide-for-medium-up">

		<section class="middle tab-bar-section">
        <h1 class="title">Main Menu</h1>
      </section>
    <section class="left-small">
      <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
    </section>
  </nav>

	<aside class="left-off-canvas-menu">
    <ul class="off-canvas-list">
      <?php 
      	// $lang is needed for both the off-canvas and main nav
        $lang = explode("/", $_SERVER['REQUEST_URI']);
				if( $lang[1] == "es" ){
				  include('includes/spanish_menu.inc.php');
				} else {
				  include('includes/english_menu.inc.php');
				}
			?>
    </ul>
	</aside>

	<a class="exit-off-canvas"></a>


	
	
  <!--  HEADER  -->
  <header id="header" class="row">
		<div id="top-row" class="large-12 columns">
			<?php print render($page['top_row']); ?>
		</div>

      <div class="medium-12 large-4 columns logo-container">
        <?php 
          if( $lang[1] == "es"){
            include('includes/spanish_logo.inc.php');
          } else {
            include('includes/english_logo.inc.php');
          }
        ?>
      </div>

    	<nav id="navigation" class="menu medium-12 large-8 columns hide-for-small">
    		<ul class="main-menu">
	      	<?php 
						if( $lang[1] == "es" ){
						  include('includes/spanish_menu.inc.php');
						} else {
						  include('includes/english_menu.inc.php');
						}
					?>
				</ul>
    	</nav> <!-- /navigation -->

  </header> <!-- /header -->

  

  <!--  MAIN  -->

  <div id="main" class="row">
    <div class="container">	    

    	<?php if (!empty($page['sidebar_first'])): ?>
        <aside id="sidebar-first" class="<?php print $sidebar_first_grid; ?> columns sidebar">
          <?php print render($page['sidebar_first']); ?>
        </aside> <!-- /sidebar-first -->
      <?php endif; ?> 

    	<section id="content" class="<?php print $main_grid; ?> main columns">
  
          <?php if ($title|| $messages || $tabs || $action_links): ?>
            <div id="content-header">

              <?php if ($page['highlighted']): ?>
                <div id="highlighted"><?php print render($page['highlighted']) ?></div>
              <?php endif; ?>

              <?php print render($title_prefix); ?>

              <?php if (!$is_front && !empty($title)): ?>
                <h1 class="title"><?php print $title; ?></h1>
              <?php endif; ?>

              <?php print render($title_suffix); ?>
              <?php print $messages; ?>
              <?php print render($page['help']); ?>

              <?php if ($tabs): ?>
                <div class="tabs"><?php print render($tabs); ?></div>
              <?php endif; ?>

              <?php if ($action_links): ?>
                <ul class="action-links"><?php print render($action_links); ?></ul>
              <?php endif; ?>

            </div> <!-- /#content-header -->
          <?php endif; ?>

          <div id="content-area">
            <?php print render($page['content']) ?>
          </div>

      </section> <!-- /content-inner /content -->

      
    </div>
  </div> <!-- /main -->

  <!--  FOOTER  -->

  <?php if ($page['footer']): ?>
    <footer id="footer" class="row">
      <div class="container large-12 columns">
      <?php print render($page['footer']); ?>
      </div>
    </footer> <!-- /footer -->
  <?php endif; ?>

</div> <!-- /.inner-wrap>
</div> <!-- /page -->
