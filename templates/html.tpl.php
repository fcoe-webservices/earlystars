<!DOCTYPE html>
<html>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $head_scripts; ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  <!--SECURE JAVASCRIPT PIXEL CODE START-->
  <script src="https://acuityplatform.com/Adserver/pxlj/1994696939389375882?" type="text/javascript" async ></script>
  <!--SECURE JAVASCRIPT PIXEL CODE END-->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div id="skip">
    <a href="#content"><?php print t('Jump to Navigation'); ?></a>
  </div>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $scripts; ?>
  <?php print $page_bottom; ?>
  
</body>
</html>