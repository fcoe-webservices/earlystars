module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      options: {
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          'css/app.css': 'scss/app.scss'
        }
      }
    },

    watch: {
      options: {
        livereload: true,
      },
      grunt: { 
        files: ['Gruntfile.js'],
        tasks: ['build'],
      },
      sass: {
        files: 'scss/**/*.scss',
        tasks: ['sass']
      }, 
      scripts: {
        files: ['js/*.js'],
        tasks: ['concat'],
        options: { 
          spawn: false
        } 
      }, 
      files: {
        files: ['templates/*.php', '*.php', 'includes/*.inc', 'fonts/*'],
        options: {
          livereload: true,  
        } 
      }
    },

    concat: {
      top: {
        src: [
          'bower_components/modernizr/modernizr.js',
        ],
        dest: 'js/production.top.js',
      },
      bottom: {
        src: [
          // Call this to include all javascript libraries
          'bower_components/foundation/js/foundation.js',
          // Call this to include the basic foundation library, then include each library seperately.
          //'bower_components/foundation/js/foundation/foundation.js',
          'js/app.js',
        ], 
        dest: 'js/production.bottom.js',
      }
    }

  });

  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('build', ['sass', 'concat']);
  grunt.registerTask('default', ['build','watch']);

}
